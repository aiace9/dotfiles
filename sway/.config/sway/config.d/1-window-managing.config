# SPDX-FileCopyrightText: (C) 2024 Ruggero Lot <ruggero90@gmail.com>
#
# SPDX-License-Identifier: CC-BY-4.0
for_window [window_type="dialog"] floating enable
for_window [window_role="dialog"] floating enable

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# This op do the upgrade
# window -> container
# split the container
bindsym $mod+semicolon split h
bindsym $mod+v split v
bindsym $mod+t split toggle

# enter full screen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
# bindsym $mod+d focus child

# do not follow mouse focus
focus_follows_mouse no

# enable popup in fullscreen
popup_during_fullscreen smart

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# kill focused window
bindsym $mod+Shift+q kill
bindsym $mod+Shift+a exec swaymsg -t get_tree | \
          jq 'recurse(.nodes[], .floating_nodes[]) | select(.focused).pid' \
          xargs -L 1 kill -9


# Local Variables:
# mode: conf-space
# End: