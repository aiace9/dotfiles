-- -*- lua -*-

local name 	= "kim-api"
local version 	= "2.3.0"

family("KIM")

whatis("Name         : " .. name)
whatis("Version      : " .. version)

local home    = os.getenv("HOME") .. "/.local/opt/software"

home=home .. "/" .. name .. "/" .. version


prepend_path("PATH", home .."/bin")
prepend_path("LD_LIBRARY_PATH", home .."/lib64")
prepend_path("LD_LIBRARY_PATH", home .."/libexec")
prepend_path("CPATH", home .."/include")
prepend_path("INCLUDE", home .."/include")
