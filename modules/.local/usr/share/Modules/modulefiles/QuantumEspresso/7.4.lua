-- -*- lua -*-

local name 	= "qe"
local version 	= "7.4"

family("QuantumEspresso")

whatis("Name         : " .. name)
whatis("Version      : " .. version)

depends_on("mpi/openmpi-x86_64")


local home    = os.getenv("HOME") .. "/.local/opt/software"

home=home .. "/" .. name .. "/" .. version


prepend_path("PATH", home .."/bin")
prepend_path("LD_LIBRARY_PATH", home .."/lib64")
prepend_path("CPATH", home .."/include")
prepend_path("INCLUDE", home .."/include")
