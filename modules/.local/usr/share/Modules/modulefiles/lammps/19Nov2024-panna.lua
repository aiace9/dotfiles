-- -*- lua -*-

local name 	= "lammps"
local version 	= "19Nov2024-panna"

family("LAMMPS")

whatis("Name         : " .. name)
whatis("Version      : " .. version)

depends_on("mpi/openmpi-x86_64","kim")


local home    = os.getenv("HOME") .. "/.local/opt/software"

home=home .. "/" .. name .. "/" .. version


prepend_path("PATH", home .. "/bin")

prepend_path("MANPATH", home .. "/share/man1")

pushenv("LAMMPS_POTENTIALS", home .. "/share/lammps/potentials")
pushenv("MSI2LMP_LIBRARY", home .. "/share/lammps/frc_files")

