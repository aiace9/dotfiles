My Dotfiles
==============

This repository is used to store all my dot files

It is optimized to be used with `stow`

**Great thanks to:**
- [This article](https://medium.com/@waterkip/managing-my-dotfiles-with-gnu-stow-262d2540a866)
- [This repository](https://github.com/toshism/dotfiles)

