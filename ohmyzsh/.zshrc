# Path to your oh-my-zsh installation.
export ZSH=$HOME/source_codes/ohmyzsh

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes

ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# increase the min values
#set history size
export HISTSIZE=70000
#save history after logout
export SAVEHIST=50000

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=$HOME/.local/lib/ohmyzsh/custom


# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

plugins=(git
         wd
         rsync
         tmux
         torrent
         nmap
         lammps
         restic
         gopass
         kubectl
         minikube
         misc-functions
         misc-aliases
         pyaliases
         xournalpp
         gpg-agent
         kubectx2
         kubeutils
        )

# User configuration is located in ZSH_CUSTOM folder.
source $ZSH/oh-my-zsh.sh

# >>>> Vagrant command completion (start)
fpath=(/opt/vagrant/embedded/gems/2.2.19/gems/vagrant-2.2.19/contrib/zsh $fpath)
compinit
# <<<<  Vagrant command completion (end)

source /home/ruggero/.config/broot/launcher/bash/br

# final operations
# export COWPATH=/usr/share/cowsay/cows # fedora+sway bug(likely, maybe solved f41)

# Module system config
module unuse /usr/share/Modules/modulefiles
module use $HOME/.local/usr/share/Modules/modulefiles
if internet_status; then
    weather=$(curl -fGsS --max-time 1 --data-urlencode "1pnQFm" wttr.in/$MY_LOCATION | head -n 16 2> /dev/null)
    ! [[ $weather =~ "timeout" ]] && echo $weather || fortune -a | cowsay -f kitty
else
    fortune -a | cowsay
fi

