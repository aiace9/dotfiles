# set PATH so it includes user's private bin if it exists
if [[ -d $HOME/bin ]] ; then
    PATH=$HOME/bin:$PATH
fi

if [[ -d $HOME/.local/bin ]] ; then
    PATH=$HOME/.local/bin:$PATH
fi

export PATH

# add free desktop std directory
# specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
XDG_CONFIG_HOME=$HOME/.config
export XDG_CONFIG_HOME
