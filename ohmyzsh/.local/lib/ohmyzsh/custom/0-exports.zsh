# setup Rust
export PATH="$HOME/.cargo/bin:$PATH"

# setup quarto
export PATH="/opt/quarto/bin:$PATH"

#set KREW_ROOT to move the binary - kubernetes 
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH" 

# config WD plugin
export WD_CONFIG=$XDG_CONFIG_HOME/.warprc

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='vim'
else
    export EDITOR='emacsclient -a vim -tty'
fi

# Sway settings
# export XDG_CURRENT_DESKTOP="sway"
# export XDG_SESSION_TYPE="wayland"

# fixing sway mess needed for screen sharing in a hacky way
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$UID/bus

# setup summon default
export SUMMON_PROVIDER_PATH=$HOME/.local/lib/summon;
export SUMMON_PROVIDER=gopass
