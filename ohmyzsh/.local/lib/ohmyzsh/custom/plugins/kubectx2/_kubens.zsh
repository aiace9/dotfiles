# SPDX-FileCopyrightText: (C) 2021 Joshua Gleitze <dev@joshuagleitze.de>
#
# SPDX-License-Identifier: Apache-2.0

#compdef kubens kns=kubens
_arguments "1: :(- $(kubectl get namespaces -o=jsonpath='{range .items[*].metadata.name}{@}{"\n"}{end}'))"
