#-------------------------------------------------------------------
# use nocorrect alias to prevent auto correct from "fixing" these
# -------------------------------------------------------------------

# COMPLETE_ALIASES
# Prevents aliases on the command line from being internally substituted
# before completion is attempted.  The effect is to make the alias a
# distinct command for completion purposes.

# global alias
alias -g G='|grep'

# miscellaneous
alias open='xdg-open'
alias dud1='du . -h --max-depth=1'

# funny alias
alias daicazzo='sudo -p "Oh, come on... alright, gimme your password:" $(fc -ln -1)'vk

# directory, better default
unalias ll
unalias l
unalias la

alias l='ls -lh'
alias ll='ls -lah'
alias lll='ls -lahZ'

alias la='ls -lA'

# Someone will hate me for this XD
alias vim='emacsclient -a vim -tty'


# kubernetes extension
# TODO add support for namespaces
alias kgetsh='f(){kubectl exec --stdin --tty $@ -- /bin/sh;  unset -f f; }; f'

# flatpak
alias drawio='flatpak run com.jgraph.drawio.desktop'
