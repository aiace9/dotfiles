
function checksum(){
    if [ -f "$1" ]; then
  	    basename $1 | sed -e "s/\(.*\)/\1 /"
	      openssl md5 $1 | sed -e "s/^MD5.*=/md5/" | sed -e "s/\(.*\)/\1 /"
	      openssl sha256 $1 | sed -e "s/^SHA256.*=/sha256/" | sed -e "s/\(.*\)/\1 /"
	      openssl sha512 $1 | sed -e "s/^SHA512.*=/sha512/" | sed -e "s/\(.*\)/\1 /"
	      openssl rmd160 $1 | sed -e "s/^R.*=/rmd160/"
    else
	      echo "Usage: $0 filename"
    fi
}

function box() {
    t="$1xxxx";
    c=${2:-=};
    echo ${t//?/$c};
    echo "$c $1 $c";
    echo ${t//?/$c};
}

function internet_status () {
	  # Test for network connection over selected interfaces
	  for interface in $(ls /sys/class/net/ | grep -E "(enp|wlp).+");
	  do
	  	  if [[ $(cat /sys/class/net/$interface:gs/@///carrier 2>/dev/null) = 1 ]]; then return 0; fi
	  done

    return 1 
}
