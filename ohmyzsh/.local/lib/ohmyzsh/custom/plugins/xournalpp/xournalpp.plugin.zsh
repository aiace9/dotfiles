# explanation
# https://unix.stackexchange.com/questions/630367/zsh-completion-only-offer-specific-file-extensions-for-completion
zstyle ':completion::complete:xournalpp:*' file-patterns '
  *(D-/):local-directories:"local directory"
  *.(xopp)(D-^/):xopp-files:"xopp files"
'
