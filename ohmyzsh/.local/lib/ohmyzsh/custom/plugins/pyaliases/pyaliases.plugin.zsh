# Python section
# virtualenv shortcut
alias mkvenv='python3 -m virtualenv .venv'
alias av='source ./.venv/bin/activate'
alias dv='deactivate'

function pyclean() {
    ZSH_PYCLEAN_PLACES=${*:-'.'}
    find ${ZSH_PYCLEAN_PLACES} -type f -name "*.py[co]" -delete
    find ${ZSH_PYCLEAN_PLACES} -type d -name "__pycache__" -delete
    find ${ZSH_PYCLEAN_PLACES} -type d -name ".mypy_cache" -delete
}
